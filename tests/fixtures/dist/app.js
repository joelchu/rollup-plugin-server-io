var dummy = (function () {
    'use strict';

    /**
     * Just a phony method for testing
     * @param {*} something pass anything
     * @return {*} return anything
     */
    function main(something) {
        console.log('You pass:', something);

        return something;
    }

    return main;

}());
//# sourceMappingURL=app.js.map
