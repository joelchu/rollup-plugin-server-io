'use strict';

var serverIoCore = require('server-io-core');

function _interopDefaultLegacy (e) { return e && typeof e === 'object' && 'default' in e ? e : { 'default': e }; }

var serverIoCore__default = /*#__PURE__*/_interopDefaultLegacy(serverIoCore);

/**
 * Our custom rollup plugin, a wrapper for server-io-core
 */
/**
 * @param {object} options pass to server-io-core
 * @return {object} for rollup
 */
function serverIo(options) {
  if ( options === void 0 ) options = {};

  // Force these options on config
  var config = Object.assign(options, {
    autoStart: false, // we must overwrite this for the plugin to work
    reload: {
      enable: true,
      verbose: false
    }
  });
  // getting the fn(s)
  var ref = serverIoCore__default['default'](config);
  var start = ref.start;
  var stop = ref.stop;
  var signals = ['SIGINT', 'SIGTERM'];
  // listen to signal and close it
  signals.forEach(function (signal) {
    process.on(signal, function () {
      stop();
      process.exit();
    });
  });
  var running;
  // return
  return {
    name: 'serverIo',
    buildEnd: function buildEnd() {
      if (!running) {
        running = true;
        start();
      }
    }
  }
}

module.exports = serverIo;
